# Slides

Comment exporter les fichiers markdowns en slides

## Getting Started
### Sur votre machine
[Installer nix](https://nixos.org/download)

### Dans le projet
```bash
 nix develop --impure
```

## Usage

Pour lancer le build des slides en continue, lancez la commande suivante:
```bash
task build -w
```
